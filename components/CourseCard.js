import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types'; //validate data
									//catch prop object
export default function CourseCard({courseProp}){
	//destructuring prop object so no need to use props.courseProp.name
	const {name, description, price, start_date, end_date} = courseProp;
			//can be anything; human readable
			//count are not affected in each courses, cause they have unique ID
	const [count, setCount] = useState(0);
			//state  state center
	const [seats, setSeats] = useState(10);
	const [isOpen, setIsOpen] = useState(true);

	function enroll(){
		setCount (count + 1);
		setSeats(seats - 1);
	}

	//useEffect watches state changes; purpose is to run when something change
	useEffect(() => {
		if(seats === 0){
			setIsOpen(false);
		}
		//2nd parameter, the 2nd parameter of useEffect is optional,pag wala palagi tatakbo yun useEffect; array cause it can count miltiple things
	}, [seats])
	//we get all the data
	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<span className="subtitle">Description</span>
					<br />
					{description}
					<br />
					<span className="subtitle">Price</span>
					<br />
					&#8369;{price}
					<br />
					<span className="subtitle">Start Date</span>
					<br />
					{start_date}
					<br />
					<span className="subtitle">End Date</span>
					<br />
					{end_date}
					<br />
					<span className="subtitle">Enrollees</span>
					<br />
					{count}
					<br />
					<span className="subtitle">Seats</span>
					<br />
					{seats}
				</Card.Text>
				{isOpen ?
					<Button className="bg-primary" onClick={enroll}>Enroll</Button>
					:
					<Button className="bg-danger" disabled>Not Available</Button>
				} 
			</Card.Body>
		</Card>
		)
}
//data validation
CourseCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}