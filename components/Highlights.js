import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(

		<Row>
			<Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Learn From Home</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad cupiditate fugiat, repudiandae fuga voluptatem iure aut, eligendi harum unde voluptas, voluptatum excepturi libero? Magni unde voluptatem similique placeat aliquid. At. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti ipsam neque repellat necessitatibus aspernatur nam accusantium exercitationem nobis cumque libero suscipit tenetur ducimus reiciendis, magni excepturi voluptas numquam assumenda esse.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>

	        <Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Study Now, Pay Later</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad cupiditate fugiat, repudiandae fuga voluptatem iure aut, eligendi harum unde voluptas, voluptatum excepturi libero? Magni unde voluptatem similique placeat aliquid. At. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti ipsam neque repellat necessitatibus aspernatur nam accusantium exercitationem nobis cumque libero suscipit tenetur ducimus reiciendis, magni excepturi voluptas numquam assumenda esse.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>

	        <Col>
	            <Card className="cardHighlight">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Be Part of Our Community</h2>
	                    </Card.Title>
	                    <Card.Text>
	                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad cupiditate fugiat, repudiandae fuga voluptatem iure aut, eligendi harum unde voluptas, voluptatum excepturi libero? Magni unde voluptatem similique placeat aliquid. At. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti ipsam neque repellat necessitatibus aspernatur nam accusantium exercitationem nobis cumque libero suscipit tenetur ducimus reiciendis, magni excepturi voluptas numquam assumenda esse.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
		</Row>
	)
}