import React from 'react';

//Create a COntext Object
const UserContext = React.createContext();

//Provide component that allows consuming components to subscribe to context changes

export const UserProvider = UserContext.Provider;

export default UserContext;